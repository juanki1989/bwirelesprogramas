<?php

$path="/var/www/location/captures/";

function listarArchivos($path)
{
	$dir = opendir($path);
	while ($elemento = readdir($dir))
	{
		if( $elemento != "." && $elemento != "..")
		{
			$elemento;
			$extension_cap="cap";
			$fecha=date("F j, Y, H:m:s");
			$ext=explode(".", $elemento);
			$mensaje="## Se ha encontrado un fichero con una extensión no válida: " . $elemento;
			$mensaje2="## Se ha tratado el fichero: " . $elemento . "correctamente.";

			//echo $ext[1];
			//echo $mensaje;
			if(strcmp($extension_cap, $ext[1])!==0)
			{
				$f = fopen("/var/www/location/logs/pcap2csv_log/extension_failed" . $fecha . "log", "a");
				fwrite($f, $mensaje . PHP_EOL);
				fclose($f);
			}
			$convertido=$ext[0] . ".csv";//Genero el nombre para el fichero convertido
			$fich="/var/www/location/captures/" . $elemento;
			$tam=filesize($fich);//Tamanyo de fichero en bytes
			//$Mb=((($tam/1024))/1024); Tamanyo de fichero en Megabytes
			$actual=date("d-m-Y H:i");
			$hf=date("d-m-Y H:i", filemtime($fich)) . "\n";
			$hfsuma=strtotime('+10 minute',strtotime($hf));
			$hf=date("d-m-Y H:i", $hfsuma);

			if($hf<$actual)//Limite de tiempo para poder convertir el fichero
			{
				if($tam>0)
				{
					$f2 = fopen("/var/www/location/logs/pcap2csv_log/extension_ok" . $fecha . "log", "a");
					fwrite($f2, $mensaje2 . PHP_EOL);
					fclose($f2);
					$copia="cp /var/www/location/csv/*.csv /var/www/location/copia_csv";
					exec($copia);
					$comando="tshark -r " . $fich . " -T fields -e wlan.sa -e frame.time_epoch -t a -e eth.src -e ip.src -e tzsp.wlan.signal frame.len | sort > /var/www/location/csv/" . $convertido;
					exec($comando);//Ejecuto el comando en la consola
					
					unlink("/var/www/location/captures/" . $elemento);//Borro el elemento tratado
				}
				else
				{
					unlink("/var/www/location/captures/" . $elemento);
				}
			}
			
		}
	
	}
	closedir($dir);//Cierro el directorio

}
listarArchivos($path);//Llamo a la fución
?>
