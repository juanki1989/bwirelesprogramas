<?php

$path="../csv/";

function listarArchivos($path)//Lista los ficheros del directorio $path
{
	$i=0;
	$dir = opendir($path);

	while ($elemento = readdir($dir))
	{
		if( $elemento != "." && $elemento != "..")
		{
			echo $elemento;//Cada vez que se encuentra un elemento se sale del bucle
			break;
		}
	}
	return $elemento;
}

//variables de fichero

$fich=listarArchivos($path);//Guarda el retorno de la función
$fp = fopen ("../csv/" . $fich, "r" ); //Concatena el directorio destino con el nombre del fichero convertido
$tam_linea=0;//Mide el tamaño de la linea row
$mac="";
$ip="";
$db="";
$epoch="";
$m1="";
$f1="";
$ip1="";
$signal1="";
$m2="";
$f2="";
$ip2="";
$signal2="";
$pasada=TRUE;
$guardar=0;
$lmac="";
$lf1="";
$lf2="";
$lip="";
$ldb="";


function guardar2($f2,&$lf2)
{
	$lf2=$f2;
}

function guardar3(&$lmac,&$lf1,&$lip,&$ldb,&$lf2,$f2,$m2,$ip2,$signal2,$padada)
{
	$lmac=$m2;
	$lf1=$f2;
	$lip=$ip2;
	$ldb=$signal2;
	unset($lf2);
	
	$pasada=FALSE;
}

//Divide el string que devuelve $row y lo separa para tratarlo, sólo la primera linea
function l1(&$m1,&$f1,&$ip1,&$signal1,$mac,$ip,$epoch,$db,&$pasada)
{
	$m1=$mac;
	$f1=$epoch;
	$ip1=$ip;
	$signal1=$db;
	$pasada=FALSE;
	unset($mac);
	unset($epoch);
	unset($ip);
	unset($db);
}
//Misma función que !l1!
function l2(&$m2,&$f2,&$ip2,&$signal2,$mac,$ip,$epoch,$db)
{
	$m2=$mac;
	$f2=$epoch;
	$ip2=$ip;
	$signal2=$db;
}

//Compara las variables de L1 y L2 para actualizar la fecha o guardar en la BD según condiciones
function comparar ($lmac,$lf1,$lip,$ldb,$m2,$f2,$ip2,$signal2,&$guardar)
{
	if($lmac==$m2) 
	{
		if($lf1!=$f2)
		{
			$guardar=2;
		}
	}
	if($lmac!=$m2)
	{
		$guardar=3;
	}
}

//Convierte las fechas epoch a fecha DATETIME() y guarda la linea completa en la BD, si no cumple la limitación de la hora, guarda la dirección MAC en un fichero .txt.
function mostrar_final ($lmac,$lf1,$lip,$ldb,$lf2)
{
	$convertirda="";
	$convertirda2="";

	$epochmenor = $lf1;
	$epochmayor = $lf2;

	$f1=date('r', $epochmenor);
	$f2=date('r', $epochmayor);

	$d1=$f1[5] . $f1[6];//dia
	$d1a=$f1[8] . $f1[9] . $f1[10];//mes en letra ej jan,feb,....
	$d1b=$f1[12] . $f1[13] . $f1[14] . $f1[15];//año
	$d1c=$f1[17] . $f1[18] . $f1[19] . $f1[20]. $f1[21] . $f1[22] . $f1[23] . $f1[24];//hora

	$d2=$f2[5] . $f2[6];//día
	$d2a=$f2[8] . $f2[9] . $f2[10];//mes en letra ej: jan,feb,....
	$d2b=$f2[12] . $f2[13] . $f2[14] . $f2[15];//año
	$d2c=$f2[17] . $f2[18] . $f2[19] . $f2[20] . $f2[21] . $f2[22] . $f2[23] . $f2[24];//hora
	
	$min1=$f1[20]. $f1[21];
	$min2=$f2[20]. $f2[21];

	if($d1a=="Jan"){$d1a="01";}
	if($d1a=="Feb"){$d1a="02";}
	if($d1a=="Mar"){$d1a="03";}
	if($d1a=="Apr"){$d1a="04";}
	if($d1a=="May"){$d1a="05";}
	if($d1a=="Jun"){$d1a="06";}
	if($d1a=="Jul"){$d1a="07";}
	if($d1a=="Aug"){$d1a="08";}
	if($d1a=="Sep"){$d1a="09";}
	if($d1a=="Oct"){$d1a="10";}
	if($d1a=="Nov"){$d1a="11";}
	if($d1a=="Dec"){$d1a="12";}

	if($d2a=="Jan"){$d2a="01";}
	if($d2a=="Feb"){$d2a="02";}
	if($d2a=="Mar"){$d2a="03";}
	if($d2a=="Apr"){$d2a="04";}
	if($d2a=="May"){$d2a="05";}
	if($d2a=="Jun"){$d2a="06";}
	if($d2a=="Jul"){$d2a="07";}
	if($d2a=="Aug"){$d2a="08";}
	if($d2a=="Sep"){$d2a="09";}
	if($d2a=="Oct"){$d2a="10";}
	if($d2a=="Nov"){$d2a="11";}
	if($d2a=="Dec"){$d2a="12";}

	$convertida=$d1b . "-". $d1a . "-" . $d1 . " " . $d1c;
	$convertida2=$d2b . "-". $d2a . "-" . $d2 . " " . $d2c;

	$a=explode(" ", $convertida);
	$b=explode(" ", $convertida2);

	$dif=$b[1]-$a[1];//Diferencia entre horas

	//if(($dif<2 && $a>$b) || ($a==$b && $min1>$min2 && $dif<2)) //si la diferencia entre horas es menor a 2 guarda en la base de datos, si no en dismissed.txt
	if($dif<2 && $lmac[2]==":" && $d1b[0]=="2" && $d2b[0]=="2")
	{
		//echo $lip . "\n"; 
//DELETE FROM `radacct` WHERE `s_strength`> -35 or `s_strength` < -85
		
		$dbhost="localhost";
		$dbusuario="dani";
		$dbpassword="pastorBW12!";
		$db="location";
		$conexion = mysql_connect($dbhost, $dbusuario, $dbpassword);
		mysql_select_db($db, $conexion) OR DIE ("Error: No es posible establecer la conexión");	
		$sql = "INSERT INTO radacct (mac, accstarttime, accstoptime, s_strength, sensor) VALUES ('$lmac', '$convertida', '$convertida2', '$ldb', '$lip')";
		//$sql = "INSERT INTO radacct (mac, accstarttime, accstoptime, s_strength, sensor) VALUES ('ab:6b:c0:5a:fa:a3', '2015-02-10 13:21:30', '2015-02-10 13:32:30', '-60' ,'192.168.1.5')";
		$result=mysql_query($sql);
		mysql_close($conexion);
	}
	else
	{
		if($lmac[2]==":")
		{
		$f = fopen("/var/www/location/dismissed/dismissed.txt", "a");
		fwrite($f, $lmac . PHP_EOL);
		fclose($f);
		}
	}
}

//Separa el string leido del CSV para pasarlo a la función l1 o l2 y tratarlo.
function separar_linea ($fila,$tam_linea,&$mac,&$epoch,&$ip,&$db)
{
	$mac=substr($fila,0,17);
	$epoch=substr($fila,18,20);
	$ip=substr($fila,40,13);
	$db=substr($fila,54,3);
}

function medir_linea (&$tam_linea, $rowP1)
{
	$tam_linea=strlen($rowP1);
}

function guardar1($m1,$f1,$ip1,$signal1,&$lmac,&$lf1,&$lip,&$ldb,&$pasada)
{
	$lmac=$m1;
	$lf1=$f1;
	$lip=$ip1;
	$ldb=$signal1;
	$pasada=FALSE;
	unset($m1);
	unset($f1);
	unset($ip1);
	unset($signal1);
}

//Lee del fichero csv linea por linea y compara la linea actual, con la siguiente, según condiciónes, es el cuerpo del programa desde donde se llaman todas las funciones.
while (( $data1 = fgetcsv ( $fp , 1000 , "," )) !== FALSE )
{
	foreach($data1 as $rowP1)
	{
		if($pasada===TRUE)
		{
			$fila=$rowP1;
			separar_linea ($fila,$tam_linea,$mac,$epoch,$ip,$db);
			l1($m1,$f1,$ip1,$signal1,$mac,$ip,$epoch,$db,$primera);
			guardar1($m1,$f1,$ip1,$signal1,$lmac,$lf1,$lip,$ldb,$pasada);
		}
		else
		{
			$fila=$rowP1;
			separar_linea ($fila,$tam_linea,$mac,$epoch,$ip,$db);
			l2($m2,$f2,$ip2,$signal2,$mac,$ip,$epoch,$db);
			comparar ($lmac,$lf1,$lip,$ldb,$m2,$f2,$ip2,$signal2,$guardar);

			if($guardar==2)//Si las Mac son iguales, actualiza la fecha de salida.
			{
				guardar2($f2,$lf2);
			}
			if($guardar==3)//Si las direcciones mac son distintas, guarda la dirección anterior
			{
				$guardar=0;
				mostrar_final($lmac,$lf1,$lip,$ldb,$lf2);
				guardar3($lmac,$lf1,$lip,$ldb,$lf2,$f2,$m2,$ip2,$signal2,$pasada);
			}
		}
	}//foreach externo
}//while externo

fclose ( $fp );//Cierra el fichero abierto
//rename("/var/www/location/csv/" . $fich , "/var/www/location/inserted2csv/" . $fich);
/*if(unlink("/var/www/location/csv/" . $fich))//Borra el fichero .csv que se ha tratado
{
	exec("php5 /var/www/location/prg/csv2sql.php");
	exec("php5 /var/www/html/neg.php");
}
*/
?>
