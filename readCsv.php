<?php

class Decodificador
{
	//variables Goblaes
	public $pathFolderCsv = "/var/www/location/csv/";
	public $visitorLimit = -75;
	public $tPrima = '00:01:00';
	public $ts = '00:00:00';
	public $tmax = '02:01:00';//tiempo maximo para que se ignore una mac(dimissing)
	//fin variables globales
	//variables de conexion
	public $dbhost="localhost";
	public $dbusuario="jcarlos";//"bwireless";
	public $dbpassword="jcarlos45%";//"bwreg457!";
	public $db="location";
	public $conexion = null;
	public $signal_array = array();
	public $t_init = null;
	public $t_pb = null;
	public $t_acc = '00:00:00';
	public $t_pb_acc = '00:00:00';
	public $veinte = '00:20:00';
	public $cinco = '00:05:00';
	public $lineaAnterior ="";
	public $listaIntervalos = array();
	public $idSensor = null;										
	//fin de variables de conexion
	public $ultimaFechaAcumulada ="";
	function main()
	{
		$path = $this->pathFolderCsv;
		echo $path;
		$dir = opendir($path);
		while ($elemento = readdir($dir))
		{
			$this->signal_array = array();
			$this->t_init = null;
			$this->t_pb = null;
			$this->t_acc = '00:00:00';
			$this->t_pb_acc = '00:00:00';
			$this->lineaAnterior ="";
			$this->listaIntervalos = array();
			$this->idSensor = null;										
			if( $elemento != "." && $elemento != "..")
			{
				echo $elemento;//Cada vez que se encuentra un elemento se sale del bucle
				$this->acumularIntervalos($elemento);
				$this->proceso2();	
			}
		}
		
	}
	/*function getSiguienteArchivo($path)//Lista los ficheros del directorio $path
	{
		$i=0;
		$dir = opendir($path);
		while ($elemento = readdir($dir))
		{
			if( $elemento != "." && $elemento != "..")
			{
				echo $elemento;//Cada vez que se encuentra un elemento se sale del bucle
				break;
			}
		}
		return $elemento;
	}*/
	function acumularIntervalos($archivoCsv)
	{
		$path = $this->pathFolderCsv;
		$archivoCsv = $archivoCsv;//$this->getSiguienteArchivo($path);
		$fp = fopen ($path . $archivoCsv, "r" );
		//echo  "</br>";
		$i = 0;

		while (( $datos = fgetcsv ( $fp , 100 , "," )) !== FALSE )//Go to next line
		{	
			$i++;		
			$linea = $this->separarLinea($datos[0]); //transformamos en lista
			//echo "$i mac $linea[0] ".$this->getDate($linea[1])." ".$linea[3]." </br>";
			if($this->esValido($linea)){
				if($this->lineaAnterior == "")// first time?
				{
					$this->lineaAnterior = $linea;// save the Mac
					$this->findIdSensor($linea[2]);
				}else{
					if($this->lineaAnterior[0] != $linea[0]) // Did the Mac change?
					{	
						if($this->t_acc > $this->ts)
							$this->saveInTheArray($this->lineaAnterior,$i);//Save in the array
						$this->inicializarVariables($linea);  //inicializar variables					
					}else{
						if($this->diferenciaTiempoEpoch($linea[1],$this->lineaAnterior[1]) > $this->tPrima) //t(n)-t(n-1)>t’
						{
						//	echo "la diferencia es entre tiempos es mayor  t' </br>";
							if($this->t_acc > $this->ts)
								$this->saveInTheArray($this->lineaAnterior,$i);//Save in the array
							$this->inicializarVariables($linea);  //inicializar variables						
						}else{
							$tn = $this->getDate($linea[1]);
							if((($tn - $this->t_init) > $this->veinte) && $this->t_init != null)//t(n)-t_init>20'?
							{
							//	echo "la diferencia entre t_init y la fecha actual es mayor a 20 t_init:".$this->t_init."  tn".$tn." </br>";
								if($this->t_acc > $this->ts)
									$this->saveInTheArray($this->lineaAnterior,$i);//Save in the array
							 	$this->inicializarVariables($linea);  //inicializar variables						
							}else{
								if($this->visitorLimit < $linea[3]) //Is the sample among visitor limits?
								{

									if( $this->visitorLimit < $this->lineaAnterior[3] )//Is the sample n-1 among visitor limits?
									{
										if($this->t_init == null) //t_init initialized?
										{
											$this->t_init = $this->getDate($this->lineaAnterior[1]);//t_init = t(n)				
									//		echo " inicializamos t_init ".$this->t_init." </br>";
										}
										$this->t_acc = $this->sumarTiempo($this->t_acc, ($this->diferenciaTiempoEpoch($linea[1],$this->lineaAnterior[1])));//t_acc=t(n)-t(n-1)+t_acc
										array_push($this->signal_array, $linea[3]); //add signal to signal array
										$this->ultimaFechaAcumulada = 	$this->getDate($linea[1]);							
									}
								}else{
									if($this->t_pb == null)	//	t_pb=0?
									{
										$this->t_pb = $this->getDate($linea[1]);//t_pb = t(n)					
									}
									$this->t_pb_acc = $this->sumarTiempo($this->t_pb_acc, ($this->diferenciaTiempoEpoch($linea[1], $this->lineaAnterior[1])));// t_pb_acc=t_pb_acc+(t(n)-t_pb) ACUMULAMOS
									if($this->t_pb_acc > $this->tPrima) //t_pb_acc>t’
									{
										$this->inicializamosPb(); //t_pb = 0; T_pb_acc = 0; 
										if($this->t_acc > $this->ts)
											$this->saveInTheArray($this->lineaAnterior,$i);//Save in the array
						 				$this->inicializarVariables($linea);  //inicializar variables		
									}
								}	
							}	
						}		
						$this->lineaAnterior = $linea; 
					}
				}				
			}
			
		}
		if($this->t_acc > $this->ts)
			$this->saveInTheArray($this->lineaAnterior,$i);//Save in the array
		fclose ( $fp );//cerramos la lectura
		if(unlink($this->pathFolderCsv . $archivoCsv))//Borra el fichero .csv que se ha tratado
		{
			//exec("php5 readCsv.php");
		//	exec("php5 /var/www/html/neg.php");
		}
		//$archivoCsv = $this->getSiguienteArchivo($path);		

	}

	function proceso2()
	{
		$macInsertada = null;
		$macAnterior = null;
		$lista = $this->listaIntervalos;
		$inicioDeLecturaMacs = 0;
	//		echo "proceso2 ". count($lista) ."</br>";	
		for($i = 0; $i < count($lista); $i++)
		{
			if($macAnterior!= $lista[$i][0])
			{
				$inicioDeLecturaMacs = $i;
			}
			//echo "fila numero $i  </br>";
			if($macInsertada != $lista[$i][0])//Was the mac(n) inserted?
			{

					$tacc = $this->acumularTiempoDentroElMarco($lista, $i);
		//			echo "acumulado $tacc </br>";
					if($tacc > $this->cinco)
					{
						//echo "$tacc ENCONTRO ".$lista[$i][0]." ".$lista[$i][2]." </br>";
						$res = $this->TiempoAcumuladoYPromedioSginalVisitor($lista, $i);
						$tacc = $res[0];
						$promedioSignal = $res[1];
						$lastDate = $res[2];
						if($tacc < $this->tmax)
							$this->insertInRadact($lista[$i][0], $tacc, $lista[$i][1], $lastDate, $promedioSignal, true, $this->idSensor);		
						$macInsertada = $lista[$i][0];
					}else{
						if($i+1 <count($lista))
						{
							if($lista[$i][0] != $lista[$i + 1][0])
							{
	//							echo "$macInsertada no encontro </br>";
								$res = $this->promedioTiempoComoPasivo($lista, $inicioDeLecturaMacs);	
								$tacc = $res[0];
								$signalprom = $res[1];
								$lastDate = $res[2];
								if($tacc < $this->tmax)
									$this->insertInRadact($lista[$i][0], $tacc, $lista[$i][1], $lastDate, $signalprom, false, $this->idSensor);		
								$macInsertada = $lista[$i][0];
							}	
						}else{
	//						echo "last mac array $macInsertada no encontro </br>";
							$res = $this->promedioTiempoComoPasivo($lista, $inicioDeLecturaMacs);	
							$tacc = $res[0];
							$signalprom = $res[1];
							$lastDate = $res[2];
							if($tacc < $this->tmax)
								$this->insertInRadact($lista[$i][0], $tacc, $lista[$i][1], $lastDate, $signalprom, false, $this->idSensor);		
							$macInsertada = $lista[$i][0];
						}
					}	
			}
			$macAnterior = 	$lista[$i][0];
		}
	}

	function promedioTiempoComoPasivo($lista, $i)
	{
		$arrayTiempoAcumulado = array();
		$arraySignal = array();
		$ultimaMac = $lista[$i][0];
		$lastDate = $lista[$i][2];
		for( $i ; $i < count($lista); $i++)
		{

			if($lista[$i][0] != $ultimaMac) //has the mac changed?
				break;	
			array_push($arrayTiempoAcumulado, $lista[$i][3]);
			array_push($arraySignal, $lista[$i][4]);
			$ultimaMac = $lista[$i][0];
		//	echo "$i acumulando ".$lista[$i][3]." </br>";
			$lastDate = $lista[$i][2];
		}
		$res[0] = date('H:i:s', array_sum(array_map('strtotime', $arrayTiempoAcumulado)) / count($arrayTiempoAcumulado));	
		$res[1] = round(array_sum($arraySignal)/count($arraySignal));   
		$res[2] = $lastDate;	
		//echo count($lista)." ".count($arrayTiempoAcumulado)." agrego tiempo $res[0] PB";
		return $res;
	}

	function esValido($linea)
	{
		$res = true;
		if($linea[0] == '00:00:00:00:00:00')
			$res = false;
		else{
			$var = split(':', $linea[0]);
			$cant = count($var); 
			if( $cant != '6')
				$res = false;
			else
				if($linea[0] == null || $linea[1] == null || $linea[2] == null || $linea[3] == null) 
					$res = false;
		}
		//if($res == false)
		//echo "ignorado";	
		return $res;
	}

	function TiempoAcumuladoYPromedioSginalVisitor($lista, $i)
	{
		$tacc = $lista[$i][3];
		$t_inicio = $lista[$i][1];
		$t_fin = $this->sumarTiempo($t_inicio, $this->veinte);
		$lastDateta = $lista[$i][2];
		$signalArray = array();
		array_push($signalArray, $lista[$i][4]);
		$ultimaMac = $lista[$i][0];
		$i++;
		
		for( $i ; $i < count($this->listaIntervalos); $i++)
		{
			if($lista[$i][0] != $ultimaMac) //has the mac changed?
				break;
			if($this->getTiempo($lista[$i][1]) < $t_fin)
			{
				$tacc = $this->sumarTiempo($tacc, $lista[$i][3]);
				array_push($signalArray, $lista[$i][4]);
				$lastDateta = $lista[$i][2];
			}else{
				if($lista[$i][3] > $this->cinco)
				{
					$tacc = $this->sumarTiempo($tacc, $lista[$i][3]); 	
					array_push($signalArray, $lista[$i][4]);
					$lastDateta = $lista[$i][2];
				}
			}
			$ultimaMac = $lista[$i][0];
		}
		$promedioSignal = array_sum($signalArray)/count($signalArray);  
		$res[0] = $tacc;
		$res[1] = round($promedioSignal);
		$res[2] = $lastDateta;
		return $res;
		//$this->insertInRadact('00:01:5c:32:98:01', '00:10:00', '2015-06-02', '-90', true, '192.168.120.5');
	}

	function acumularTiempoDentroElMarco($lista, $i)
	{
		$tacc = $lista[$i][3];
		$t_inicio = $this->getTiempo($lista[$i][1]);
		$t_fin = $this->sumarTiempo($t_inicio, $this->veinte);
		$i++;
		for($i ; $i < count($lista); $i++)
		{
			if($lista[$i][0] != $lista[$i-1][0]) //has the mac changed?
				break;
			if($tacc >= $this->cinco)
				break;
			//echo $this->getTiempo($lista[$i][1])." < hora fin : $t_fin </br>";
			if($this->getTiempo($lista[$i][1]) < $t_fin)
			{
				if($this->getTiempo($lista[$i][2]) < $t_fin)
				{
		//			echo "deberiamos acumular  </br>";
					$tacc = $this->sumarTiempo($tacc, $lista[$i][3]); 
				}else{
		//			echo "deberiamos acumular con resta </br>";
					$tacc = $this->sumarTiempo($tacc, $this->diferenciaTiempo($t_fin, $lista[$i][1])); 
				}		
			}else{
				break;
			}	
		}
		return $tacc;
	}

	function imprrimirListaIntervalos()
	{
		$this->acumularIntervalos();
		$datos = $this->listaIntervalos;
		for($i = 0; $i < count($this->listaIntervalos); $i++)
		{
			echo " ".$datos[$i][0]." ".$datos[$i][1]." ".$datos[$i][2]." ".$datos[$i][3]." ".$datos[$i][4]." ".$datos[$i][5]." </br>";
		}
	}

	function inicializamosPb()
	{
		$this->t_pb = null;
		$this->t_pb_acc = '00:00:00';
	}

	function saveInTheArray($datosAInsertar, $i)
	{
		
		//echo "</br>insetando ".$datosAInsertar[0]." num $i </br>";
		$avg_signal = $this->getSignalArrayPromedio($this->signal_array);
		$datos[0] = $datosAInsertar[0];
		$datos[1] = $this->t_init;
		$datos[2] = $this->ultimaFechaAcumulada;
		$datos[3] = $this->t_acc;
		$datos[4] = $avg_signal;
		$datos[5] = $datosAInsertar[2];
		array_push($this->listaIntervalos, $datos);		
		
	//	echo "</br>insetando ".$datosAInsertar[0]." ".$datos[1]." ".$datos[2]." ".$datos[3]." num $i </br>";
	//	$avg_signal = $this->getSignalArrayPromedio($this->signal_array);
//		$this->insertInArray($datosAInsertar[0], $this->t_acc, $this->t_init, $this->ultimaFechaAcumulada, $avg_signal, $datosAInsertar[2]);		
	}

	function inicializarVariables($linea)
	{
		$this->lineaAnterior = $linea; 
		$this->signal_array = array();
		$this->t_init = null;//$this->getDate($linea[1]);
		$this->t_acc = '00:00:00';				
	}

	function addTaccToMac($t_acc, $signal_array, $mac, $sensor)
	{
	//	echo "acumuladoo $t_acc a $mac </br>";
		$avg_signal = $this->getSignalArrayPromedio($signal_array);
		$this->openConexion();
		$sql = "UPDATE `radacct` 
				SET acc_time = ADDTIME(acc_time, '$t_acc'), 
				    s_strength_avg = ((s_strength_avg + ('$avg_signal'))/2) 
				WHERE mac = '$mac'
				AND sensor = '$sensor'";	
		$result = mysql_query($sql);
		$this->closeConexion();
	}
	
	function existeMacInBD($mac)
	{
		//muy importante quitar DATE( accstarttime ) = CURDATE( )  en caso de procesar arvhivos de otras fechas 
		$res = false;
		$this->openConexion();
		$sql = "SELECT * 
				FROM radacct
				WHERE 
				 mac =  '$mac'";
				//echo $sql;	
		$result = mysql_query($sql);
		if (mysql_num_rows($result)>0)
			$res = true;
		$this->closeConexion();
		return $res;
	}

	function diferenciaTiempo($hora1, $hora2)
	{
		$accrued_time = strtotime($hora1);
  		$current_session = strtotime($hora2) - strtotime("00:00:00");
  		$res =  date("H:i:s", ($accrued_time - $current_session));
  		return $res;
	}

	function diferenciaTiempoEpoch($date1, $date2)
	{
		$d1 = new DateTime($this->getDate($date1));
		$d2 = new DateTime($this->getDate($date2));
		$dteDiff = $d1->diff($d2);
		$dteDiff = $dteDiff->format("%H:%I:%S");
		return $dteDiff;
	}

	function sumarTiempo($date1, $date2)
	{
		$accrued_time = strtotime($date1);
  		$current_session = strtotime($date2) - strtotime("00:00:00");
  		$res =  date("H:i:s", ($accrued_time + $current_session));
  		return $res;
	}

	function getTiempo($date)
	{
		$current_session = strtotime($date);
  		$res =  date("H:i:s", ($current_session));
  		return $res;
	}

	function insertarDatos($datosAInsertar, $i)
	{
		//echo "</br>insetando ".$datosAInsertar[0]." num $i </br>";
		$avg_signal = $this->getSignalArrayPromedio($this->signal_array);
		$this->insertInArray($datosAInsertar[0], $this->t_acc, $this->t_init, $this->getDate($datosAInsertar[1]), $avg_signal, $datosAInsertar[2]);		
	}

	function getSignalArrayPromedio($signal_array)
	{
		if(count($signal_array) > 0)
			$avg_signal = ((array_sum($signal_array))/count($signal_array));
		else
			$avg_signal = 0;
		return $avg_signal;
	}
	function getDate($epoch)
	{
		//echo $epoch;
		return date("Y-m-d H:i:s", $epoch);
	}
	function insertInArray($mac, $acc_time, $t_ini,$t_fin, $avg_signal, $sensor)
	{
		$this->openConexion();
		$sql = "INSERT INTO array (mac, spenttime, accstarttime, accstoptime, s_strength_avg, sensor) 
					   VALUES ('$mac','$acc_time','$t_ini', '$t_fin', '$avg_signal', '$sensor')";	
		//echo $sql."</br>";
		$result=mysql_query($sql);
		$this->closeConexion();
	}

	function insertInRadact($mac, $spent_time, $accstarttime, $accstoptime, $av_signal, $visitor, $idSensor)
	{
		$this->openConexion();
		$sql = "INSERT INTO radacct (mac, accstarttime, accstoptime, acctime, avsignal, visitor, sensorcode) 
					   VALUES ('$mac', '$accstarttime', '$accstoptime', '$spent_time', '$av_signal', '$visitor', '$idSensor')";	
		echo $sql."</br>";
		$result=mysql_query($sql);
		$this->closeConexion();
	}

	function findIdSensor($ipSensor)
	{
		$this->openConexion();
		$sql = "SELECT sensor_code FROM `sensors` WHERE ip ='$ipSensor'";	
		echo $sql."</br>";
		$result=mysql_query($sql);
		$res=mysql_fetch_row($result);
		$this->idSensor = $res[0];
		$this->closeConexion();
	}

	function openConexion()
	{
		$this->conexion = mysql_connect($this->dbhost, $this->dbusuario, $this->dbpassword);
		mysql_select_db($this->db, $this->conexion) OR DIE ("Error: No es posible establecer la conexión");	
	}

	function closeConexion()
	{
		mysql_close($this->conexion);
	}

	

	function separarLinea ($fila)
	{
		$lista[0]=substr($fila,0,17);
		$lista[1]=substr($fila,18,20);
		$lista[2]=substr($fila,40,13);
		$lista[3]=substr($fila,54,3);
		return $lista;
	}
}

$decodificador = new Decodificador();
try {
	$decodificador->main();

	//echo $decodificador->esValido('00:00:00:00:00:00');
	//$decodificador->acumularIntervalos();
	//$decodificador->imprrimirListaIntervalos();
	//$decodificador->getDate('1427955908.851820000');
	//echo $decodificador-> existeMacInBD('00:18:4d:ff:ff:07');
	//$decodificador->diferencia('2015-06-04 02:02:00','2015-06-02 00:00:00');
	//$decodificador->sumarTiempo('02:02:00','00:10:00');
	//$decodificador->insertInRadact('00:01:5c:32:98:01', '00:10:00', '2015-06-02', '-90', true, '192.168.120.5');
	//echo $decodificador->getTiempo('2015-06-02 02:02:00');
} catch (Exception $e) {
	echo 'Excepción capturada: ',  $e->getMessage(), "\n";
}

?>